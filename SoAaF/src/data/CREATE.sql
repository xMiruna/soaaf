CREATE TABLE T_Personen(
P_name	VARCHAR(10)		PRIMARY KEY
);

CREATE TABLE T_Votes(
P_vote_id	VARCHAR(10)		PRIMARY KEY,
F_name	VARCHAR(10),
CONSTRAINT	fk_name	FOREIGN KEY 	REFERENCES	T_Personen(P_name),
F_musik_id	VARCHAR(10),
CONSTRAINT 	fk_musik_id	FOREIGN KEY	REFERENCES	T_Musikwuensche(P_musik_id),
);

CREATE TABLE T_Musikwuensche(
P_musik_Id	VARCHAR(10)		PRIMARY KEY,
interpret	VARCHAR(30),
titelname	VARCHAR(50),
genre		VARCHAR(50),
);
