CREATE DATABASE soaaf;
USE soaaf;
CREATE TABLE `T_Personen` (
  `P_name` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`P_name`));
CREATE TABLE `T_Musikwuensche` (
  `P_musik_id` VARCHAR(10) NOT NULL,
  `interpret` VARCHAR(30) NULL,
  `titelname` VARCHAR(50) NULL,
  `genre` VARCHAR(50) NULL,
  PRIMARY KEY (`P_musik_id`));

CREATE TABLE `T_Votes` (
  `P_vote_id` VARCHAR(10) NOT NULL,
  `F_name` VARCHAR(10) NULL,
  `F_musik_id` VARCHAR(10) NULL,
  PRIMARY KEY (`P_vote_id`),
  INDEX `fk_name_idx` (`F_name` ASC),
  INDEX `fk_musik_id_idx` (`F_musik_id` ASC),
  CONSTRAINT `fk_name`
    FOREIGN KEY (`F_name`)
    REFERENCES `T_Personen` (`P_name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_musik_id`
    FOREIGN KEY (`F_musik_id`)
    REFERENCES `T_Musikwuensche` (`P_musik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

USE `soaaf`;
INSERT INTO `T_Personen` (`P_name`) VALUES ('Tyrion');
INSERT INTO `T_Personen` (`P_name`) VALUES ('Jamie');
INSERT INTO `T_Personen` (`P_name`) VALUES ('Cersei');
INSERT INTO `T_Personen` (`P_name`) VALUES ('Chrissy');
INSERT INTO `T_Personen` (`P_name`) VALUES ('Tine');
INSERT INTO `T_Personen` (`P_name`) VALUES ('Nick');
INSERT INTO `T_Personen` (`P_name`) VALUES ('Fedi');

USE `soaaf`;
INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES ('10000', 'Avenged Sevenfold', 'Carry on', 'Power Rock/Metal');
INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES ('10001', 'Metallica', 'Seek and Destroy', 'Metal');
INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES ('10010', 'Disturbed', 'Ten Thousand Fist', 'Metal');
INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES ('10100', 'Bruno Mars', 'Just The Way You Are', 'Pop');
INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES ('11000', 'Lady Gaga', 'Poker Face', 'Artpop');
INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES ('11001', 'Rita Ora', 'Anywhere', 'Pop');
INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES ('11010', 'Alan Walker', 'Faded', 'Pop');

USE `soaaf`;
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00001', 'Tyrion', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00002', 'Jamie', '10100');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00003', 'Cersei', '11010');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00004', 'Chrissy', '10100');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00005', 'Tine', '11000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00006', 'Nick', '11001');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00007', 'Fedi', '11010');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00008', 'Tyrion', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00009', 'Fedi', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00010', 'Tyrion', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00011', 'Tyrion', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00012', 'Tyrion', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00013', 'Tyrion', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00009', 'Fedi', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00009', 'Fedi', '10001');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00004', 'Chrissy', '11000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00009', 'Fedi', '10001');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00006', 'Nick', '10001');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00006', 'Nick', '10000');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00010', 'Tyrion', '10001');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00003', 'Cersei', '10010');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00002', 'Jamie', '10001');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00002', 'Jamie', '10001');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00003', 'Cersei', '11010');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00003', 'Cersei', '11010');
INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('00003', 'Cersei', '11010');

ALTER TABLE T_Personen ADD hausname VARCHAR(30) NULL;
