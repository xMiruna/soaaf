package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import logik.Musikwunsch;
import logik.Person;
import logik.Votes;

public class DBMusikvoting {
	// private int maxWert; // Attribute
	//
	// public DBMusikvoting(int maxWert) { // Methoden
	// this.maxWert = maxWert;
	//
	// }
	//
	// String driver = "com.mysql.jdbc.Driver"; // Verbindung zur DB herstellen
	// Class.forName(driver);
	// String url = "jdbc:mysql://localhost/soaaf";
	// String user = "Tyrion";
	// String passwort = "mypasswort";
	// Connection con;con=DriverManager.getConnection(url,user,passwort);
	//
	// public int getMaxWert() { // Getter und Setter
	// return maxWert;
	// }
	//
	// public void setMaxWert(int maxWert) { // MaxWert von den Votes aus DB nehmen
	// this.maxWert = maxWert;
	// }
	//
	// public String getNaechsteVoteId() { //N�chste VoteID hinzuf�gen und spiechern
	// int neachsteVoteId = Votes.liesInt(); //Gibt Vote_Id aus der Vote-Klasse
	// wieder
	// char voteID = Votes.getVoteID;
	// int i = Integer.parseInt(String.valueOf(voteID)); // Varchar in Int umwandeln
	// }if(naechsteVoteId>=0;i++)
	//
	// {
	// System.out.println(naechsteVoteId); // hinzuf�gen eines neuen Votes
	// }
	// int naechsteVoteId = 0; // Alternativer Weg?
	// while(naechsteVoteId>=0)
	// {
	// System.out.println(naechsteVoteId++);
	//
	// }
	// }

	public String getMaxVoteID() {

		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";
		String maxVoteID = "";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT MAX(P_Vote_id) FROM T_Votes;");

			// Ergebnis abfragen
			while (rs.next()) {
				// maximaler Wert aus der DB holen
				maxVoteID = rs.getString(1);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return maxVoteID;
	}
	// Die n�chste Vote_ID ausgeben lassen

	public String getNaechsteVoteID() {
		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";
		String naechsteVoteID = "";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT MAX(P_Vote_id) FROM T_Votes;");
			while (rs.next()) {
				naechsteVoteID = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("N�chste VoteID = " + naechsteVoteID);
		return naechsteVoteID;
	}

	// Neuer Vote hinzuf�gen
	public void voteHinzufuegen(Votes v) {
		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";

		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			Statement stmt = con.createStatement();
			stmt.executeUpdate("INSERT INTO `T_Votes` (`P_vote_id`, `F_name`, `F_musik_id`) VALUES ('" + v.getVoteID()
					+ "', '" + v.getPerson().getName() + "', '" + v.getMusikwunsch().getMusikID() + "');");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Musikwunsch[] getPlaylist() {

		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";
		Musikwunsch[] musikwunsche = new Musikwunsch[100];
		int i = 0;

		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*), interpret, titelname,genre\r\n" + "FROM t_votes AS v\r\n"
					+ "INNER JOIN\r\n" + "t_musikwuensche AS m ON m.P_musik_id = v.F_musik_id\r\n"
					+ "GROUP BY interpret, titelname, genre  \r\n" + "ORDER BY `COUNT(*)`  DESC");

			// Ergebnis abfragen
			while (rs.next()) {
				Musikwunsch tmp = new Musikwunsch(rs.getString(4), rs.getString(2), rs.getString(3));
				musikwunsche[i] = tmp;
				i++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return musikwunsche;

	}

	public Musikwunsch[] MusiktitelAusgabe() {
		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";
		int i = 0;
		Musikwunsch[] mw = new Musikwunsch[100];
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM T_Musikwuensche;");
			while (rs.next()) {
				mw[i++] = new Musikwunsch(rs.getString(4), rs.getString(2), rs.getString(3));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mw;
	}

	public Person NutzerAusgabe(String name) {
		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";
		int i = 0;
		Person na = null;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM T_Person WHERE P_Name = '"+name+"';");
			while (rs.next()) {
				na = new Person(rs.getString(1), rs.getString(2));
	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return na;
	}
	
	public void musikEintragen(Musikwunsch m) {
		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";

		
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			PreparedStatement stmt = con.prepareStatement("INSERT INTO `T_Musikwuensche` (`P_musik_id`, `interpret`, `titelname`, `genre`) VALUES (?,?,?,?)");
			stmt.setInt(1,m.getMusikID());
			stmt.setString(2, m.getBandName());
			stmt.setString(3, m.getTitelName());
			stmt.setString(4, m.getGenre());
			stmt.execute();
		
		}  catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void nameEintragen(Person p) {
		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";

		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			PreparedStatement stmt = con.prepareStatement("INSERT INTO `T_Personen`(`P_Name`) VALUES (?)");
			stmt.setString(1,p.getName());
			stmt.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void AdelstitelHinzufuegen(Person p) {				//Adelstitel hinzuf�gen
		String url = "jdbc:mysql://localhost/soaaf?";
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";
		int i = 0;
		Person ah = null;

		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			PreparedStatement stmt = con.prepareStatement("UPDATE T_Personen SET hausname = 'ser' WHERE P_Name LIKE 'Tyrion' ");		//Ver�nderung der Tabelle T_Personen
			stmt.setString(1, p.getHausname());
			stmt.setString(2, p.getName());
			stmt.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		DBMusikvoting dbm = new DBMusikvoting();
		Musikwunsch[] mw = dbm.getPlaylist();

		for (int i = 0; i < mw.length; i++) {
			if (mw[i] != null)
				System.out.println(mw[i]);
		}

	}
}