package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Startseite extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Startseite frame = new Startseite();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Startseite() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 496, 622);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(5, 1, 0, 0));
		
		JLabel label = new JLabel("");
		ImageIcon icon = new ImageIcon(Startseite.class.getResource("/bilder/samusik1.PNG"));
		icon.setImage(icon.getImage().getScaledInstance(490, 110, Image.SCALE_DEFAULT));
		label.setIcon(icon);
		contentPane.add(label);
		
		JButton btnAnmelden = new JButton("Anmelden");
		btnAnmelden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Anmelden anmelden2 = new Anmelden();
				anmelden2.setVisible(true);
				setVisible(false);
			}
		});
		contentPane.add(btnAnmelden);
		
		JButton btnVoten = new JButton("Voten");
		btnVoten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Voting voting2 = new Voting();
				voting2.setVisible(true);
				setVisible(false);	
			}
		});
		contentPane.add(btnVoten);
		
		JButton btnGastgeber = new JButton("Gastgeber");
		btnGastgeber.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GastgeberEinloggen einloggen2 = new GastgeberEinloggen ();
				einloggen2.setVisible(true);
				setVisible(false);
			}
		});
		contentPane.add(btnGastgeber);
		
		JButton btnMusikwunschEintragen = new JButton("Musikwunsch eintragen");
		btnMusikwunschEintragen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				musikwunschEintragen_Clicked();
			}
		});
		contentPane.add(btnMusikwunschEintragen);
	}

	public void musikwunschEintragen_Clicked() {
		Musikwuensche mw = new Musikwuensche();
		mw.setVisible(true);
	}
	
}
