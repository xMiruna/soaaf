package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import data.DBMusikvoting;
import logik.Person;

import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Anmelden extends JFrame {

	private JPanel contentPane;
	private JTextField tfdAnmelden;
	private JTextField tfdBestaetigen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Anmelden frame = new Anmelden();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Anmelden() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		tfdAnmelden = new JTextField();
		contentPane.add(tfdAnmelden);
		tfdAnmelden.setColumns(10);
		
		tfdAnmelden = new JTextField();				//Neuer TextField f�r den Adelstitel
		contentPane.add(tfdAnmelden);
		tfdAnmelden.setColumns(10);
		
		JButton btnAnmelden = new JButton("anmelden");	
		btnAnmelden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person p = new Person(tfdAnmelden.getText(), tfdBestaetigen.getText()); 
				DBMusikvoting v = new DBMusikvoting();
				v.nameEintragen(p);
				System.out.println(p);
			}
		});
		contentPane.add(btnAnmelden);
	};

}