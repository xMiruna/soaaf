package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import data.DBMusikvoting;
import logik.Musikwunsch;
import logik.Person;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Musikwuensche extends JFrame {

	private JPanel contentPane;
	private JTextField tfd_bandname;
	private JTextField tfd_titelname;
	private JTextField tfd_genre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Musikwuensche frame = new Musikwuensche();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Musikwuensche() {
		setTitle("Musikwunsch");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblNewLabel = new JLabel("MusikwŁnsche eintragen");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		contentPane.add(lblNewLabel, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(7, 2, 0, 0));

		JLabel lblNewLabel_2 = new JLabel("Bandname: ");
		panel.add(lblNewLabel_2);

		tfd_bandname = new JTextField();
		tfd_bandname.setHorizontalAlignment(SwingConstants.LEFT);
		tfd_bandname.setFont(new Font("Tahoma", Font.PLAIN, 11));
		panel.add(tfd_bandname);
		tfd_bandname.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Titelname: ");
		panel.add(lblNewLabel_3);

		tfd_titelname = new JTextField();
		panel.add(tfd_titelname);
		tfd_titelname.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Genre: ");
		panel.add(lblNewLabel_1);

		tfd_genre = new JTextField();
		panel.add(tfd_genre);
		tfd_genre.setColumns(10);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JButton btn_hinzufeugen = new JButton("+");
		btn_hinzufeugen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Musikwunsch m = new Musikwunsch(tfd_genre.getText(), tfd_bandname.getText(),
						tfd_titelname.getText());
					DBMusikvoting v = new DBMusikvoting();
					v.musikEintragen(m);
				System.out.println(m);
			}
		});
		btn_hinzufeugen.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn_hinzufeugen.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(btn_hinzufeugen);
	}

}
