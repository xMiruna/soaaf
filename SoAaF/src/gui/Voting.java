package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import data.DBMusikvoting;
import logik.Musikwunsch;

import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Voting extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnVoten;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Voting frame = new Voting();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Voting() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setOpaque(false);
		table.setColumnSelectionAllowed(true);
		table.setModel(new DefaultTableModel(
			Musikwunsch.tabellenanzeige((new DBMusikvoting()).MusiktitelAusgabe()),
			new String[] {
				"vote_id", "titelname", "interpret", "genre"
			}
		));
		
		contentPane.add(table, BorderLayout.CENTER);
		
		btnVoten = new JButton("Voten");
		btnVoten.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
			//	Votes v = new Votes ()
				DBMusikvoting vb = new DBMusikvoting();
			//	vb.voteHinzufuegen(v);
				int SongId = Integer.parseInt((String) table.getModel().getValueAt(1,0));
				vb.getNaechsteVoteID();
				System.out.println(table.getSelectionModel().getMinSelectionIndex());
				
			}
			
			
		});
		contentPane.add(btnVoten, BorderLayout.SOUTH);
	}
}
