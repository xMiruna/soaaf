package logik;

public class Person {
	private String name;
	private String hausname;			//Adelstitel als Attribut
	
	public Person (String name, String hausname) {
		this.name = name;
		this.hausname = hausname;
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setHausname (String hausname) {		//Getter und Setter
		this.hausname = hausname;
	}
	public String getHausname() {
		return hausname;
	}
	@Override
	
	public String toString() {
		return "Person [name=" + name + " , hausname=" + hausname+"]";	
	} 
	//Ausgabe in einen String
	
	
	public boolean IstAdelig() {
	if(hausname == "adelig" || hausname == "ser") { 				// IF Bedingung zur Überprüfung des Titels 
		return true;
	}
}
}
		



		
		
