package logik;

public class Musikwunsch {
	private String genre;
	private String bandName;
	private String titelName;
	private int musikID;

	public Musikwunsch(String genre, String bandName, String titelName) {
		this.genre = genre;
		this.bandName = bandName;
		this.titelName = titelName;

	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getMusikID() {
		return musikID;
	}

	public void setMusikID(int musikID) {
		this.musikID = musikID;
	}

	public String getBandName() {
		return bandName;
	}

	public void setBandName(String bandName) {
		this.bandName = bandName;
	}

	public String getTitelName() {
		return titelName;
	}

	public void setTitelName(String titelName) {
		this.titelName = titelName;
	}

	@Override
	public String toString() {
		return "Musikwunsch [genre=" + genre + ", musikID=" + musikID + ", bandName=" + bandName + ", titelName="
				+ titelName + "]";
	}

	public static Object[][] tabellenanzeige(Musikwunsch[] musikwuensche) {
		Object[][] tabelle = new Object[100][4];
		for (int i = 0; i < 100 && musikwuensche[i] != null; i++) {
			tabelle[i][0] = musikwuensche[i].getMusikID();
			tabelle[i][1] = musikwuensche[i].getTitelName();
			tabelle[i][2] = musikwuensche[i].getBandName();
			tabelle[i][3] = musikwuensche[i].getGenre();
//			System.out.println(tabelle[i] + " ");
			
		}
		return tabelle;
	}
}
