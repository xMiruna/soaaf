package logik;

import data.DBMusikvoting;

public class Votes {
	private int voteID;
	private Musikwunsch musikwunsch;
	private Person person;
	
	
	public Votes (int voteID, Musikwunsch musikwunsch, Person person) {
		this.voteID = voteID;
		this.musikwunsch = musikwunsch;
		this.person = person;
	}
	public int getVoteID() {
		return voteID;
	}
	public void setVoteID(int voteID) {
		this.voteID = voteID;
	}
	public Musikwunsch getMusikwunsch() {
		return musikwunsch;
	}
	public void setMusikwunsch(Musikwunsch musikwunsch) {
		this.musikwunsch = musikwunsch;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}

	public String naechsteVoteId() {
		DBMusikvoting dbmv = new DBMusikvoting();
		int x = Integer.parseInt(dbmv.getMaxVoteID());
		x++;
		String s = x + " ";
		return s;
	}
}
